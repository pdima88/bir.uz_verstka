$(document).ready(function(){

	$('body').on('click', function(event){
		if(!$(event.target).closest('.menu-container').length){
			$('.top-menu').removeClass('active');
		}
	});
	$('.top-menu-switch').on('click', function(){
		$('.top-menu').toggleClass('active');
	});

	if($('.comments-slider').length){
		$('.comments-slider').slick({
			prevArrow: '<span class="icon icon-slider-arrow-l"></span>',
			nextArrow: '<span class="icon icon-slider-arrow-r"></span>',
			autoplay: true,
			arrows: true,
			dots: false
		});
	}

	if($('#my-calendar').length){
		$('#my-calendar').fullCalendar({
		    //theme: true,
		    titleFormat: 'MM YYYY',
		    firstDay: 1,
		    height: 'auto',
		    header: {
				left: '',
				center: '',
				right: ''
			},
			footer: {
				left: '',
				center: '',
				right: ''
			},
			locale: 'ru',
			views: {
		        month: { // name of view
		            //titleFormat: 'YYYY, MM, DD'
		            // other view-specific options here
		        }
		    },
		    viewRender: function(view, element){
		    	var dataArr = view.title.split(' ');

				var m = moment(view.title, 'MM YYYY');
				m.locale('ru');
		    	var monthName = m.format('MMMM');
		    	var monthNum = m.format('MM');
		    	var yearNum = m.format('YYYY');
		    	$('.calendar-month-num').text(monthNum);
		    	$('.calendar-month-name').text(monthName);
		    	$('.calendar-month-title').text(monthName);
		    	$('.calendar-year').text(yearNum);
		    }
		});
		$('.calendar-arrow').on('click', function(event){
			event.preventDefault();
			if($(this).hasClass('calendar-prev')){
				$('#my-calendar').fullCalendar('prev');
			}
			if($(this).hasClass('calendar-next')){
				$('#my-calendar').fullCalendar('next');
			}
		});
	}


}); //ready end
